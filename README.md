# Nejaušdatu testēšanas risinājumu pielietojums _Bluetooth Low Energy_ ierīču ievainojamību identificēšanai

Automātu modelēšanas solī iegūtie automāti ir atrodami sadaļā [results/automati](results/automati)

Sadaļā [results](results) ir atrodami arī darbā aprakstīto nejaušdatu testēšanas soļu izvades faili

Visiem skriptiem pamatā tiek izmantots _Pferscher et al._ izstrādātais testēšanas rīks
https://git.ist.tugraz.at/apferscher/ble-fuzzing
kurā tiek veiktas modifikācijas nepieciešamās funkcionalitātes implementācijai

Sadaļā [tools](tools) ir atrodamas darbā izmantotās rīku versijas:

### iTAG-2
iTAG-2 testēšanu izdevās veikt ar modifikāciju failā `constant.py`
iestatot `CONNECTION_ERROR_ATTEMPTS = 100`

Izmantotā rīka versija: [tools/ble-fuzzing-itag2](tools/ble-fuzzing-itag2)

 _ChM = 0_ ievainojamības replicēšanai tika modificēts pieļauto vērtību intervāls
 [tools/ble-fuzzing-itag2/fuzzing/FuzzingBLESULChM.py](tools/ble-fuzzing-itag2/fuzzing/FuzzingBLESULChM.py)
 funkcijā `def connection_request_chM` 

### Pebble 
Lai testētu ar pauzi tika izmantota versija 
[tools/ble-fuzzing-with-suspend-pebble](tools/ble-fuzzing-with-suspend-pebble)

Modifikācijas ir redzamas klasē [BLESUL.py](tools/ble-fuzzing-with-suspend-pebble/BLESUL.py) (rindiņas 353-361)
un [FuzzingBLESUL.py](tools/ble-fuzzing-with-suspend-pebble/fuzzing/FuzzingBLESUL.py) (rindiņas 710-718)

Testēšana veiksmīgāk izdevās, kad, izmantojot noklusējuma versiju [tools/ble-fuzzing-vanilla](tools/ble-fuzzing-vanilla)
paralēli periodiski sūtīja _BLE_ paketes ar izstrādātu skriptu [tools/ble-fuzzing-utils/ble_ping.py](tools/ble-fuzzing-utils/ble_ping.py)

Iegūtie automāti ar abām versijām ir atrodami [results/pebble-ping-parbaude](results/pebble-ping-parbaude)
### Vivo-4
Testēšana izdevās, izmantojot noklusējuma versiju [tools/ble-fuzzing-vanilla](tools/ble-fuzzing-vanilla)

## Palīgrīki

### Atkārtota pēdējo _n_ pakešu sūtīšana
[ble_replay.py](tools/ble-fuzzing-utils/ble_replay.py): lai ierīcei atkārtoti nosūtītu atbilstīgas _BLE_ datu paketes vadoties pēc _Pferscher et al._
risinājuma standartizvades pieejamās informācijas:
```
tools/ble-fuzzing-utils/ble_replay.py PORTS BLE_MAC IEVADE N
```
kur 

    `PORTS` atbilst _nRF52840_ spraudņa portam

    `BLE_MAC` atbilst testējamās ierīces `BLE_MAC` adresei

    `IEVADE` atbilst automātu modelēšanas solī iegūtajai standartizvadei
    
    `N` atbilst pēdējo pakešu skaitam, ko atkārtoti nosūtīt testējamai ierīcei

### Pēdējā testa piemēra sūtīšana (no testēšanas pārskata)
[ble_replay_crash.py](tools/ble-fuzzing-utils/ble_replay_crash.py): lai ierīcei atkārtoti nosūtītu pēdējo testa piemēru no vienas testēšanas sesijas:
```
tools/ble-fuzzing-utils/ble_replay_crash.py PORTS BLE_MAC fuzzing_report.txt PCAP_OUT
```

kur 

    `PORTS` atbilst _nRF52840_ spraudņa portam

    `BLE_MAC` atbilst testējamās ierīces `BLE_MAC` adresei

    `fuzzing_report.txt` atbilst automātu testēšanas solī iegūtajam testa piemēra pārskatam 
    
    `PCAP_OUT` atbilst failam, kurā tiks saglabāta testa piemēra sūtīšanas informācija _pcap_ formātā


### Ierīces sasniedzamības pārbaudes skripts
[ble_ping.py]((tools/ble-fuzzing-utils/ble_ping.py) Lai pārbaudītu, vai ierīce ir sasniedzama, tai vienreiz vai periodiski tika sūtītas _BLE_
paketes kā definēts skriptā:
```
python3 tools/ble-fuzzing-utils/ble_ping.py PORTS BLE_MAC INTERVĀLS
```

kur 

    `PORTS` atbilst _nRF52840_ spraudņa portam

    `BLE_MAC` atbilst testējamās ierīces `BLE_MAC` adresei

    `INTERVĀLS` atbilst laika periodam sekundēs, pēc kura atkārtoti tiek sūtītas _BLE_ paketes

### Eksperimentu pārvaldība
Eksperimentu pārvaldībai izmantotais kods: [run_experiment.rb](run_experiment.rb)

### Simulācijas
Simulācijās izmantotais kods ir atrodams [tools/ble-fuzzing-simulation/simulation.py](tools/ble-fuzzing-simulation/simulation.py)

Katras ierīces automātam iegūtie rezultāti: [tools/ble-fuzzing-simulation/results](tools/ble-fuzzing-simulation/results)

Simulācijas rezultātu analīze: [tools/ble-fuzzing-simulation/statistics.py](tools/ble-fuzzing-simulation/statistics.py)
