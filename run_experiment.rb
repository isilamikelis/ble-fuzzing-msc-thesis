require 'fileutils'

time = Time.new
current_date = "#{time.year}%02d%02d" % [time.month, time.day]
puts current_date

#(1..3).each do |i|
  #res_outname = "results/vivosmart4-#{current_date}/vivosmart4-#{current_date}-#{i}"
  #dirname = File.dirname res_outname
  #puts dirname
  #if !Dir.exist? dirname
    #FileUtils.mkdir_p dirname
  #end
    
  #system "python3 tools/ble-fuzzing/ble_learning.py /dev/ttyACM0 C6:16:96:A9:91:FC #{res_outname}"
#end

#dotname = "results/vivosmart4-20240401/vivosmart4-20240401-1.dot"
#dirname = "results/vivosmart4-20240401/fuzzing-1"
#pcapfname = "vivosmart4-20240401-1"
#if !Dir.exist? dirname
  #FileUtils.mkdir_p dirname
#end
  
#system "python3 tools/ble-fuzzing/ble_fuzzing.py #{dotname} /dev/ttyACM0 C6:16:96:A9:91:FC #{dirname}/ #{pcapfname}"

#(1..10).each do |i|
  #res_outname = "results/bluetoothkb/bluetoothkb-20240401-#{i}"
  #dirname = File.dirname res_outname
  #puts dirname
  #if !Dir.exist? dirname
    #FileUtils.mkdir_p dirname
  #end
    
  #system "python3 tools/ble-fuzzing/ble_learning.py /dev/ttyACM0 54:46:6B:01:67:9A #{res_outname}"
#end

def learn_device(devport, devaddr, devname)
  time = Time.new
  current_date = "#{time.year}%02d%02d" % [time.month, time.day]
  i = 1
  out_basename = "#{devname}-#{current_date}-#{i}"
  res_outname = "results/#{devname}/#{out_basename}/#{out_basename}"
  dirname = File.dirname res_outname
  while File.exist? dirname
    i += 1
    out_basename = "#{devname}-#{current_date}-#{i}"
    res_outname = "results/#{devname}/#{out_basename}/#{out_basename}"
    dirname = File.dirname res_outname
  end
  puts dirname
  #if File.file? "#{res_outname}.dot"
    #puts "#{res_outname} already exists"
    #next
  #end
  puts "save as #{res_outname}"
  if !Dir.exist? dirname
    FileUtils.mkdir_p dirname
  end
    
  system "python3 tools/ble-fuzzing/ble_learning.py #{devport} #{devaddr} #{res_outname} > #{res_outname}.log.stdout 2> #{res_outname}.log.stderr"
end

def fuzz_device(devport, devaddr, dotfile)
  i = 1
  projdir = File.dirname dotfile
  resbasename =  File.basename dotfile
  resbasename.gsub!(".dot","")
  puts projdir
  puts resbasename
  outname = "#{resbasename}-fuzzing-#{i}"
  outdir = "#{projdir}/#{outname}"
  while File.exist? outdir
    i += 1
    outname = "#{resbasename}-fuzzing-#{i}"
    outdir = "#{projdir}/#{outname}"
  end
  FileUtils.mkdir_p outdir
    
  system "python3 tools/ble-fuzzing/ble_fuzzing.py #{dotfile} #{devport} #{devaddr} #{outdir}/ #{outname} > #{outdir}/#{outname}.log.stdout 2> #{outdir}/#{outname}.log.stderr"
end

def learn_device_itag2(devport, devaddr, devname)
  time = Time.new
  current_date = "#{time.year}%02d%02d" % [time.month, time.day]
  i = 1
  out_basename = "#{devname}-#{current_date}-#{i}"
  res_outname = "results/#{devname}/#{out_basename}/#{out_basename}"
  dirname = File.dirname res_outname
  while File.exist? dirname
    i += 1
    out_basename = "#{devname}-#{current_date}-#{i}"
    res_outname = "results/#{devname}/#{out_basename}/#{out_basename}"
    dirname = File.dirname res_outname
  end
  puts dirname
  #if File.file? "#{res_outname}.dot"
    #puts "#{res_outname} already exists"
    #next
  #end
  puts "save as #{res_outname}"
  if !Dir.exist? dirname
    FileUtils.mkdir_p dirname
  end
    
  system "python3 tools/ble-fuzzing-itag2/ble_learning.py #{devport} #{devaddr} #{res_outname} > #{res_outname}.log.stdout 2> #{res_outname}.log.stderr"
end


def fuzz_device_itag2(devport, devaddr, dotfile)
  i = 1
  projdir = File.dirname dotfile
  resbasename =  File.basename dotfile
  resbasename.gsub!(".dot","")
  puts projdir
  puts resbasename
  outname = "#{resbasename}-fuzzing-#{i}"
  outdir = "#{projdir}/#{outname}"
  while File.exist? outdir
    i += 1
    outname = "#{resbasename}-fuzzing-#{i}"
    outdir = "#{projdir}/#{outname}"
  end
  FileUtils.mkdir_p outdir
    
  system "python3 tools/ble-fuzzing-itag2/ble_fuzzing.py #{dotfile} #{devport} #{devaddr} #{outdir}/ #{outname} > #{outdir}/#{outname}.log.stdout 2> #{outdir}/#{outname}.log.stderr"
end

def fuzz_chM_device_itag2(devport, devaddr, dotfile)
  i = 1
  projdir = File.dirname dotfile
  resbasename =  File.basename dotfile
  resbasename.gsub!(".dot","")
  puts projdir
  puts resbasename
  outname = "#{resbasename}-fuzzing-#{i}"
  outdir = "#{projdir}/#{outname}"
  while File.exist? outdir
    i += 1
    outname = "#{resbasename}-fuzzing-#{i}"
    outdir = "#{projdir}/#{outname}"
  end
  FileUtils.mkdir_p outdir
    
  system "python3 tools/ble-fuzzing-itag2/ble_fuzzing_chM0.py #{dotfile} #{devport} #{devaddr} #{outdir}/ #{outname} > #{outdir}/#{outname}.log.stdout 2> #{outdir}/#{outname}.log.stderr"
end

def learn_device_suspend(devport, devaddr, devname)
  time = Time.new
  current_date = "#{time.year}%02d%02d" % [time.month, time.day]
  i = 1
  out_basename = "#{devname}-#{current_date}-#{i}"
  res_outname = "results/#{devname}/#{out_basename}/#{out_basename}"
  dirname = File.dirname res_outname
  while File.exist? dirname
    i += 1
    out_basename = "#{devname}-#{current_date}-#{i}"
    res_outname = "results/#{devname}/#{out_basename}/#{out_basename}"
    dirname = File.dirname res_outname
  end
  puts dirname
  #if File.file? "#{res_outname}.dot"
    #puts "#{res_outname} already exists"
    #next
  #end
  puts "save as #{res_outname}"
  if !Dir.exist? dirname
    FileUtils.mkdir_p dirname
  end
    
  system "python3 tools/ble-fuzzing-with-suspend/ble_learning.py #{devport} #{devaddr} #{res_outname} > #{res_outname}.log.stdout 2> #{res_outname}.log.stderr"
end

def learn_device_suspend_itag1(devport, devaddr, devname)
  time = Time.new
  current_date = "#{time.year}%02d%02d" % [time.month, time.day]
  i = 1
  out_basename = "#{devname}-#{current_date}-#{i}"
  res_outname = "results/#{devname}/#{out_basename}/#{out_basename}"
  dirname = File.dirname res_outname
  while File.exist? dirname
    i += 1
    out_basename = "#{devname}-#{current_date}-#{i}"
    res_outname = "results/#{devname}/#{out_basename}/#{out_basename}"
    dirname = File.dirname res_outname
  end
  puts dirname
  #if File.file? "#{res_outname}.dot"
    #puts "#{res_outname} already exists"
    #next
  #end
  puts "save as #{res_outname}"
  if !Dir.exist? dirname
    FileUtils.mkdir_p dirname
  end
    
  system "python3 tools/ble-fuzzing-with-suspend-itag1/ble_learning.py #{devport} #{devaddr} #{res_outname} > #{res_outname}.log.stdout 2> #{res_outname}.log.stderr"
end

def fuzz_device_suspend_itag1(devport, devaddr, dotfile)
  i = 1
  projdir = File.dirname dotfile
  resbasename =  File.basename dotfile
  resbasename.gsub!(".dot","")
  puts projdir
  puts resbasename
  outname = "#{resbasename}-fuzzing-#{i}"
  outdir = "#{projdir}/#{outname}"
  while File.exist? outdir
    i += 1
    outname = "#{resbasename}-fuzzing-#{i}"
    outdir = "#{projdir}/#{outname}"
  end
  FileUtils.mkdir_p outdir
    
  system "python3 tools/ble-fuzzing-with-suspend-itag1/ble_fuzzing.py #{dotfile} #{devport} #{devaddr} #{outdir}/ #{outname} > #{outdir}/#{outname}.log.stdout 2> #{outdir}/#{outname}.log.stderr"
end

def fuzz_device_suspend_itag1_chM0(devport, devaddr, dotfile)
  i = 1
  projdir = File.dirname dotfile
  resbasename =  File.basename dotfile
  resbasename.gsub!(".dot","")
  puts projdir
  puts resbasename
  outname = "#{resbasename}-fuzzing-#{i}"
  outdir = "#{projdir}/#{outname}"
  while File.exist? outdir
    i += 1
    outname = "#{resbasename}-fuzzing-#{i}"
    outdir = "#{projdir}/#{outname}"
  end
  FileUtils.mkdir_p outdir
    
  system "python3 tools/ble-fuzzing-with-suspend-itag1-chM0/ble_fuzzing.py #{dotfile} #{devport} #{devaddr} #{outdir}/ #{outname} > #{outdir}/#{outname}.log.stdout 2> #{outdir}/#{outname}.log.stderr"
end

def learn_and_fuzz_device(devport, devaddr, devname)
  time = Time.new
  current_date = "#{time.year}%02d%02d" % [time.month, time.day]
  (1..1).each do |i|
    out_basename = "#{devname}-#{current_date}-#{i}"
    res_outname = "results/#{devname}/#{out_basename}/#{out_basename}"
    dirname = File.dirname res_outname
    puts dirname
    if File.file? "#{res_outname}.dot"
      puts "#{res_outname} already exists"
      next
    end
    puts "save as #{res_outname}"
    if !Dir.exist? dirname
      FileUtils.mkdir_p dirname
    end
      
    system "python3 tools/ble-fuzzing/ble_learning.py #{devport} #{devaddr} #{res_outname} > #{res_outname}.log.stdout 2> #{res_outname}.log.stderr"
    (1..1).each do |j|
      dotname = "#{res_outname}.dot"
      outdir = "#{res_outname}-fuzzing"
      outname = "#{out_basename}-fuzzing_session-#{j}"
      if !Dir.exist? outdir
        FileUtils.mkdir_p outdir
      end
      
      system "python3 tools/ble-fuzzing/ble_fuzzing.py #{dotname} #{devport} #{devaddr} #{outdir}/ #{outname} > #{outdir}/#{outname}.log.stdout 2> #{outdir}/#{outname}.log.stderr"
    end
  end
end

def fuzz_device_pebble(devport, devaddr, dotfile)
  i = 1
  projdir = File.dirname dotfile
  resbasename =  File.basename dotfile
  resbasename.gsub!(".dot","")
  puts projdir
  puts resbasename
  outname = "#{resbasename}-fuzzing-#{i}"
  outdir = "#{projdir}/#{outname}"
  while File.exist? outdir
    i += 1
    outname = "#{resbasename}-fuzzing-#{i}"
    outdir = "#{projdir}/#{outname}"
  end
  FileUtils.mkdir_p outdir
    
  system "python3 tools/ble-fuzzing-pebble/ble_fuzzing.py #{dotfile} #{devport} #{devaddr} #{outdir}/ #{outname} > #{outdir}/#{outname}.log.stdout 2> #{outdir}/#{outname}.log.stderr"
end

def learn_and_fuzz_pebble(devport, devaddr)
  # devport /dev/ttyACM0
  unless File.exist? devport
    abort "#{devport} does no exist"
  end
  devname = "pebble-v3"
  time = Time.new
  current_date = "#{time.year}%02d%02d" % [time.month, time.day]
  (1..1).each do |i|
    res_outname = "results/#{devname}/#{devname}-#{current_date}-#{i}/#{devname}-#{current_date}-#{i}"
    dirname = File.dirname res_outname
    puts res_outname
    if File.file? "#{res_outname}.dot"
      puts "#{res_outname} already exists"
      next
    end
    puts "save as #{res_outname}"
    if !Dir.exist? dirname
      FileUtils.mkdir_p dirname
    end
      
    system "python3 tools/ble-fuzzing-for-pebble-v3/ble_learning.py #{devport} #{devaddr} #{res_outname} > #{res_outname}.log.stdout 2> #{res_outname}.log.stderr"
    (1..1).each do |j|
      dotname = "#{res_outname}.dot"
      outdir = "#{res_outname}-fuzzing"
      outname = "#{devname}-#{current_date}-#{i}-fuzzing_session-#{j}"
      if !Dir.exist? outdir
        FileUtils.mkdir_p outdir
      end
      
      system "python3 tools/ble-fuzzing-for-pebble-v3/ble_fuzzing.py #{dotname} #{devport} #{devaddr} #{outdir}/ #{outname} > #{outdir}/#{outname}.log.stdout 2> #{outdir}/#{outname}.log.stderr"
    end
  end
end

def learn_and_fuzz_pebble_no_pair(devport, devaddr)
  # devport /dev/ttyACM0
  unless File.exist? devport
    abort "#{devport} does no exist"
  end
  devname = "pebble-no-pair"
  time = Time.new
  current_date = "#{time.year}%02d%02d" % [time.month, time.day]
  (1..1).each do |i|
    res_outname = "results/#{devname}/#{devname}-#{current_date}-#{i}/#{devname}-#{current_date}-#{i}"
    dirname = File.dirname res_outname
    puts res_outname
    if File.file? "#{res_outname}.dot"
      puts "#{res_outname} already exists"
      next
    end
    puts "save as #{res_outname}"
    if !Dir.exist? dirname
      FileUtils.mkdir_p dirname
    end
      
    system "python3 tools/ble-fuzzing-pebble-find-params/ble_learning_no_pairing.py #{devport} #{devaddr} #{res_outname} > #{res_outname}.log.stdout 2> #{res_outname}.log.stderr"
    (1..1).each do |j|
      dotname = "#{res_outname}.dot"
      outdir = "#{res_outname}-fuzzing"
      outname = "#{devname}-#{current_date}-#{i}-fuzzing_session-#{j}"
      if !Dir.exist? outdir
        FileUtils.mkdir_p outdir
      end
      
      system "python3 tools/ble-fuzzing-pebble-find-params/ble_fuzzing.py #{dotname} #{devport} #{devaddr} #{outdir}/ #{outname} > #{outdir}/#{outname}.log.stdout 2> #{outdir}/#{outname}.log.stderr"
    end
  end
end

def learn_and_fuzz_device_with_suspend(devport, devaddr, devname)
  time = Time.new
  current_date = "#{time.year}%02d%02d" % [time.month, time.day]
  (1..1).each do |i|
    out_basename = "#{devname}-#{current_date}-#{i}"
    res_outname = "results/#{devname}/#{out_basename}/#{out_basename}"
    dirname = File.dirname res_outname
    puts dirname
    if File.file? "#{res_outname}.log.stdout"
      puts "#{res_outname} already exists"
      next
    end
    puts "save as #{res_outname}"
    if !Dir.exist? dirname
      FileUtils.mkdir_p dirname
    end
      
    system "python3 tools/ble-fuzzing-with-suspend/ble_learning.py #{devport} #{devaddr} #{res_outname} 2> #{res_outname}.log.stderr | tee #{res_outname}.log.stdout "
    (1..1).each do |j|
      dotname = "#{res_outname}.dot"
      outdir = "#{res_outname}-fuzzing"
      outname = "#{out_basename}-fuzzing_session-#{j}"
      if !Dir.exist? outdir
        FileUtils.mkdir_p outdir
      end
      
      system "python3 tools/ble-fuzzing-with-suspend/ble_fuzzing.py #{dotname} #{devport} #{devaddr} #{outdir}/ #{outname} 2> #{outdir}/#{outname}.log.stderr | tee #{outdir}/#{outname}.log.stdout"
    end
  end
end

def learn_and_fuzz_device_with_suspend_proba(devport, devaddr, devname)
  time = Time.new
  current_date = "#{time.year}%02d%02d" % [time.month, time.day]
  (1..1).each do |i|
    out_basename = "#{devname}-#{current_date}-#{i}"
    res_outname = "results/#{devname}/#{out_basename}/#{out_basename}"
    dirname = File.dirname res_outname
    puts dirname
    if File.file? "#{res_outname}.log.stdout"
      puts "#{res_outname} already exists"
      next
    end
    puts "save as #{res_outname}"
    if !Dir.exist? dirname
      FileUtils.mkdir_p dirname
    end
      
    system "python3 tools/ble-fuzzing-with-suspend-proba/ble_learning.py #{devport} #{devaddr} #{res_outname} 2> #{res_outname}.log.stderr | tee #{res_outname}.log.stdout "
    #(1..1).each do |j|
      #dotname = "#{res_outname}.dot"
      #outdir = "#{res_outname}-fuzzing"
      #outname = "#{out_basename}-fuzzing_session-#{j}"
      #if !Dir.exist? outdir
        #FileUtils.mkdir_p outdir
      #end
      
      #system "python3 tools/ble-fuzzing-with-suspend/ble_fuzzing.py #{dotname} #{devport} #{devaddr} #{outdir}/ #{outname} 2> #{outdir}/#{outname}.log.stderr | tee #{outdir}/#{outname}.log.stdout"
    #end
  end
end

def learn_device_vivo4(devport, devaddr, devname)
  time = Time.new
  current_date = "#{time.year}%02d%02d" % [time.month, time.day]
  i = 1
  out_basename = "#{devname}-#{current_date}-#{i}"
  res_outname = "results/#{devname}/#{out_basename}/#{out_basename}"
  dirname = File.dirname res_outname
  while File.exist? dirname
    i += 1
    out_basename = "#{devname}-#{current_date}-#{i}"
    res_outname = "results/#{devname}/#{out_basename}/#{out_basename}"
    dirname = File.dirname res_outname
  end
  puts dirname
  #if File.file? "#{res_outname}.dot"
    #puts "#{res_outname} already exists"
    #next
  #end
  puts "save as #{res_outname}"
  if !Dir.exist? dirname
    FileUtils.mkdir_p dirname
  end
    
  system "python3 tools/ble-fuzzing-vivo4/ble_learning.py #{devport} #{devaddr} #{res_outname} > #{res_outname}.log.stdout 2> #{res_outname}.log.stderr"
end

def learn_device_pebble(devport, devaddr, devname)
  time = Time.new
  current_date = "#{time.year}%02d%02d" % [time.month, time.day]
  i = 1
  out_basename = "#{devname}-#{current_date}-#{i}"
  res_outname = "results/#{devname}/#{out_basename}/#{out_basename}"
  dirname = File.dirname res_outname
  while File.exist? dirname
    i += 1
    out_basename = "#{devname}-#{current_date}-#{i}"
    res_outname = "results/#{devname}/#{out_basename}/#{out_basename}"
    dirname = File.dirname res_outname
  end
  puts dirname
  #if File.file? "#{res_outname}.dot"
    #puts "#{res_outname} already exists"
    #next
  #end
  puts "save as #{res_outname}"
  if !Dir.exist? dirname
    FileUtils.mkdir_p dirname
  end
    
  system "python3 tools/ble-fuzzing-pebble/ble_learning.py #{devport} #{devaddr} #{res_outname} > #{res_outname}.log.stdout 2> #{res_outname}.log.stderr"
end
# learn_device("whitetag", "FC:A8:9B:02:63:92")
# learn_device("greenITag", "FF:FF:92:89:CD:80")
#learn_device("greenITagSquare","FF:FF:2D:9F:E2:80")

#learn_device_pebble('D7:89:A1:4C:50:C6')
#fuzz_device_pebble('D7:89:A1:4C:50:C6', 'results/pebble/pebble-20240407-1.dot', 'results/pebble/pebble-20240407-1-fuzz', 'pebble-20240407-1-fuzz-2')

# learn_and_fuzz_device_faster_learner('C6:16:96:A9:91:FC', 'vivosmart4')
#
# 20240417:
# pirmais fuzz
# otrais fuzz
# learn_and_fuzz_pebble('/dev/ttyACM0', 'D7:89:A1:4C:50:C6')
#learn_and_fuzz_device('/dev/ttyACM0', '54:46:6B:01:67:9A', 'keeb2')
# learn_and_fuzz_device('/dev/ttyACM0', 'D7:89:A1:4C:50:C6', 'pebble-standard')
#learn_and_fuzz_device('/dev/ttyACM0', 'FF:FF:2D:9F:E2:80', 'iTAGSquare')
# learn_and_fuzz_device('/dev/ttyACM0', 'FF:FF:92:89:CD:80', 'iTAGPear')
# learn_and_fuzz_device('/dev/ttyACM0', 'FF:FF:2D:9F:E2:80', 'iTAGSquare')
# learn_and_fuzz_device('/dev/ttyACM0', 'FF:FF:00:00:38:F5', 'blueTag1')
# learn_and_fuzz_device('/dev/ttyACM0', 'B8:27:EB:42:83:75', 'raspberrypi-zero-w')
# learn_and_fuzz_pebble_no_pair('/dev/ttyACM0', 'D3:C6:08:80:D0:A8')

# running:
# learn_and_fuzz_device('/dev/ttyACM1', 'C6:16:96:A9:91:FC', 'vivosmart4')
# learn_and_fuzz_device('/dev/ttyACM0', "E0:80:8D:FB:D8:24", 'fitbit-c3')
#if ARGV[0] == "vivo"
  #learn_and_fuzz_device('/dev/ttyACM0', 'C6:16:96:A9:91:FC', 'vivosmart4')
#elsif ARGV[0] == "vivo_suspend"
  #learn_and_fuzz_device_with_suspend('/dev/ttyACM1', 'E3:10:D5:35:97:28', 'vivo-suspend')
#elsif ARGV[0] == "vivo_suspend_prepaired"
  #learn_and_fuzz_device_with_suspend('/dev/ttyACM0', 'D1:A6:12:DF:2F:F8', 'vivo-suspend-prepaired')
#elsif ARGV[0] == "whiteTag_suspend"
  #learn_and_fuzz_device_with_suspend('/dev/ttyACM0', 'FF:FF:44:07:EA:C0', 'whiteTag-suspend')
#elsif ARGV[0] == "mle15_suspend"
  #learn_and_fuzz_device_with_suspend('/dev/ttyACM0', 'FF:FF:00:00:8D:90', 'mle15-suspend')
#elsif ARGV[0] == "mle15_prepair"
  #learn_and_fuzz_device_with_suspend('/dev/ttyACM0', 'FF:FF:00:00:8D:90', 'mle15-suspend-prepaired')
#elsif ARGV[0] == "mle15_suspend_proba"
  #learn_and_fuzz_device_with_suspend_proba('/dev/ttyACM0', 'FF:FF:00:00:8D:90', 'mle15-suspend-proba')
#elsif ARGV[0] == "pebble_no_pair"
  #learn_and_fuzz_pebble_no_pair('/dev/ttyACM1', 'D1:13:CE:32:E1:A0')
#elsif ARGV[0] == "pebble_suspend"
  #learn_and_fuzz_device_with_suspend('/dev/ttyACM1', 'D1:13:CE:32:E1:A0', 'pebble-suspend')
#else
  #puts "#{ARGV[0]} not recognized"
#end
case ARGV[0]
when 'learn'
  learn_device(ARGV[1], ARGV[2], ARGV[3])
when 'learn-suspend'
  learn_device_suspend(ARGV[1], ARGV[2], ARGV[3])
when 'learn-suspend-itag1'
  learn_device_suspend_itag1(ARGV[1], ARGV[2], ARGV[3])
when 'fuzz-suspend-itag1'
  fuzz_device_suspend_itag1(ARGV[1], ARGV[2], ARGV[3])
when 'fuzz-suspend-itag1-chM0'
  fuzz_device_suspend_itag1_chM0(ARGV[1], ARGV[2], ARGV[3])
when 'fuzz'
  fuzz_device(ARGV[1], ARGV[2], ARGV[3])
when 'learn-itag2'
  learn_device_itag2(ARGV[1], ARGV[2], ARGV[3])
when 'fuzz-itag2'
  fuzz_device_itag2(ARGV[1], ARGV[2], ARGV[3])
when 'fuzz-chM-itag2'
  fuzz_chM_device_itag2(ARGV[1], ARGV[2], ARGV[3])
when 'learn-vivo4'
  learn_device_vivo4(ARGV[1], ARGV[2], ARGV[3])
when 'learn-pebble'
  learn_device_pebble(ARGV[1], ARGV[2], ARGV[3])
when 'fuzz-pebble'
  fuzz_device_pebble(ARGV[1], ARGV[2], ARGV[3])
else 
  puts 'ARGV[0] not recognized'
end
