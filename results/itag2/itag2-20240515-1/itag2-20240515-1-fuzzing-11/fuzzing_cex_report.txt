FUZZING-CEX (0) detected on feature_req detected:
--------------------
Error inserting prefix: () and fuzzed suffix ['feature_rsp[feature_set: le_encryption+ext_scan_filter+le_ping+le_data_len_ext+rx_mod_idx+conn_par_req_proc+tx_mod_idx+ext_reject_ind+le_ext_adv+ll_privacy+ch_sel_alg+le_pwr_class+le_periodic_adv+le_2m_phy+slave_init_feat_exch+le_coded_phy]'] and suffix ['feature_rsp', 'version_req', 'feature_req', 'connection_req', 'feature_req']
Conflict detected: BTLE|BTLE_CTRL|BTLE_DATA|LL_FEATURE_RSP vs BTLE|BTLE_DATA
Expected output: ['Empty', 'Empty', 'Empty', 'Empty', 'BTLE|BTLE_DATA', 'BTLE|BTLE_CTRL|BTLE_DATA|LL_FEATURE_RSP']
Received output: ['Empty', 'Empty', 'Empty', 'Empty', 'BTLE|BTLE_DATA', 'BTLE|BTLE_DATA']
--------------------
State is UNKNOWN!
Observed Outputs: {('feature_req',): ('BTLE|BTLE_DATA',), ('feature_rsp',): ('BTLE|BTLE_DATA',), ('connection_req',): ('BTLE|BTLE_CTRL|BTLE_DATA|LL_FEATURE_RSP',), ('length_rsp',): ('BTLE|BTLE_CTRL|BTLE_DATA|LL_FEATURE_RSP',), ('mtu_req',): ('BTLE|BTLE_CTRL|BTLE_DATA|LL_FEATURE_RSP',), ('length_req',): ('BTLE|BTLE_CTRL|BTLE_DATA|LL_FEATURE_RSP',), ('pairing_req',): ('BTLE|BTLE_CTRL|BTLE_DATA|LL_FEATURE_RSP',), ('version_req',): ('BTLE|BTLE_DATA',), ('scan_req',): ('ERROR',)}




------------------------------------------------------------------------



FUZZING-CEX (1) detected on feature_req detected:
--------------------
Error inserting prefix: () and fuzzed suffix ['pairing_req[max_key_size: 0]'] and suffix ['scan_req', 'length_req', 'version_req', 'connection_req', 'feature_req']
Conflict detected: BTLE|BTLE_CTRL|BTLE_DATA|LL_FEATURE_RSP vs BTLE|BTLE_DATA
Expected output: ['Empty', 'Adv', 'Empty', 'Empty', 'BTLE|BTLE_DATA', 'BTLE|BTLE_CTRL|BTLE_DATA|LL_FEATURE_RSP']
Received output: ['Empty', 'Adv', 'Empty', 'Empty', 'BTLE|BTLE_DATA', 'BTLE|BTLE_DATA']
--------------------
state cannot be calculated since outputs cannot reproduced.
------------------------------------------------------------------------



FUZZING-CEX (2) detected on length_rsp detected:
--------------------
Error inserting prefix: () and fuzzed suffix ['connection_req[timeout: 0]'] and suffix ['length_rsp']
Conflict detected: BTLE|BTLE_CTRL|BTLE_DATA|LL_UNKNOWN_RSP vs Empty
Expected output: ['BTLE|BTLE_DATA', 'BTLE|BTLE_CTRL|BTLE_DATA|LL_UNKNOWN_RSP']
Received output: ['BTLE|BTLE_DATA', 'Empty']
--------------------
Source state: s0
Entered state: s0
Expected state: s1
Entered state is DIFFERENT. Current state: s0, Expected state: s1
------------------------------------------------------------------------



FUZZING-CEX (3) detected on length_req detected:
--------------------
Error inserting prefix: () and fuzzed suffix ['feature_req[feature_set: ]'] and suffix ['length_req', 'scan_req', 'scan_req', 'connection_req', 'length_req']
Conflict detected: BTLE|BTLE_CTRL|BTLE_DATA|LL_UNKNOWN_RSP vs BTLE|BTLE_DATA
Expected output: ['Empty', 'Empty', 'Adv', 'Adv', 'BTLE|BTLE_DATA', 'BTLE|BTLE_CTRL|BTLE_DATA|LL_UNKNOWN_RSP']
Received output: ['Empty', 'Empty', 'Adv', 'Adv', 'BTLE|BTLE_DATA', 'BTLE|BTLE_DATA']
--------------------
state cannot be calculated since outputs cannot reproduced.
------------------------------------------------------------------------



