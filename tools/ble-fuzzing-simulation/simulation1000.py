import sys
from math import ceil

from fuzzing.FuzzingBLESUL import FuzzingBLESUL
from fuzzing.FuzzingEqOracle import FuzzingEqOracle
from fuzzing.FuzzingEqOracleDryRun import FuzzingEqOracleDryRun
from fuzzing.Utils import perform_stateful_fuzzing, create_reports
from aalpy.utils import load_automaton_from_file
import random as rand
from collections import Counter
import json
from BLESULMock import BLESULMock

# automaton_path = 'dotfiles/vivosmart4-prepaired-vanilla-run-20240507-1.dot'
automaton_path = sys.argv[1]
outprefix = sys.argv[2]

hypothesis = load_automaton_from_file(automaton_path, 'mealy', compute_prefixes=True)


alphabet = ['scan_req', 'connection_req', 'length_req', 'length_rsp',  'feature_rsp', 'version_req', 'mtu_req', 'pairing_req', 'feature_req']

counts = []
for j in range(100000):
    print(j)
    hypothesis.reset_to_initial()
    states = []
    entered_packet = []
    for i in range(1000):
        states.append(hypothesis.current_state.state_id)
        pkt = rand.sample(alphabet, 1)[0]
        entered_packet.append(pkt)
        hypothesis.step(pkt)

    counts.append(Counter(states))

counts_dict = [dict(x) for x in counts]

with open(f'{outprefix}.1k_by_1k.json','w') as fh:
    final = json.dumps(counts_dict, indent=2)
    fh.write(final)

# blesul = BLESULMock()

# # fuzzer_counts = []
# fuzzer_before_counts = []
# fuzzer_after_counts = []

# for i in range(1):
    # query_num = 100000

    # states_num = len(hypothesis.states)
    # suffix_length = 5 if states_num < 5 else states_num

    # fuzz_oracle = FuzzingEqOracleDryRun(alphabet, blesul, walks_per_state=ceil(query_num / states_num), fuzzing_walk_len=1, walk_len=suffix_length, state_analysis = True)


    # fuzz_oracle.find_cex(hypothesis)

    # fuzzer_before_counts.append(Counter(fuzz_oracle.hypothesis_states_before_fuzzing))
    # fuzzer_after_counts.append(Counter(fuzz_oracle.hypothesis_states_after_fuzzing))

# fuzzer_before_counts_dict = [dict(x) for x in fuzzer_before_counts]
# fuzzer_after_counts_dict = [dict(x) for x in fuzzer_after_counts]

# with open(f'{outprefix}.fuzzer10000k_before.json','w') as fh:
    # final = json.dumps(fuzzer_before_counts_dict, indent=2)
    # fh.write(final)
# with open(f'{outprefix}.fuzzer10000k_after.json','w') as fh:
    # final = json.dumps(fuzzer_after_counts_dict, indent=2)
    # fh.write(final)
