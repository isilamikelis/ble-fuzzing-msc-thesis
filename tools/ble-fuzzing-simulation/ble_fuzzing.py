import sys
from math import ceil

from fuzzing.FuzzingBLESUL import FuzzingBLESUL
from fuzzing.FuzzingEqOracle import FuzzingEqOracle
from fuzzing.Utils import perform_stateful_fuzzing, create_reports
from aalpy.utils import load_automaton_from_file
import random as rand
from collections import Counter


automaton_path = 'dotfiles/vivosmart4-prepaired-vanilla-run-20240507-1.dot'
hypothesis = load_automaton_from_file(automaton_path, 'mealy', compute_prefixes=True)

query_num = 1000

states_num = len(hypothesis.states)
print(states_num)

suffix_length = 5 if states_num < 5 else states_num

fuzzing_sul = FuzzingBLESUL(serial_port, advertiser_address)

alphabet = ['scan_req', 'connection_req', 'length_req', 'length_rsp',  'feature_rsp', 'version_req', 'mtu_req', 'pairing_req', 'feature_req']

hypothesis.reset_to_initial()
states = []
entered_packet = []
for i in range(1000):
    states.append(hypothesis.current_state.state_id)
    pkt = rand.sample(alphabet, 1)[0]
    entered_packet.append(pkt)
    hypothesis.step(pkt)

Counter(states)

# no pairing
# alphabet = ['scan_req', 'connection_req', 'length_req', 'length_rsp',  'feature_rsp', 'version_req', 'mtu_req', 'feature_req']

# no feature
# alphabet = ['scan_req', 'connection_req', 'length_req', 'length_rsp',  'feature_rsp', 'version_req', 'mtu_req', 'pairing_req']

# no length
#alphabet = ['scan_req', 'connection_req', 'length_rsp',  'feature_rsp', 'version_req', 'mtu_req', 'pairing_req', 'feature_req']

eq_oracle = FuzzingEqOracle(alphabet, fuzzing_sul, walks_per_state=ceil(query_num / states_num), fuzzing_walk_len=1, walk_len=suffix_length, pcap_file_name=data_directory + pcap_filename, state_analysis = True)

perform_stateful_fuzzing(eq_oracle, hypothesis)

create_reports(data_directory, eq_oracle.fuzzing_report, eq_oracle.fuzzing_overall_report)



