import json
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from collections import defaultdict

def aggregate_data(h0):
    states = h0[0].keys()
    return {s:[x[s] for x in  h0] for s in states}

def draw_plot(h0, h1_values):
    simulated_packets = 100000

    state_data = aggregate_data(h0)
    medians = []
    min_values = []
    max_values = []
    states = sorted(state_data.keys())
    for s in states:
        medians.append(np.median(state_data[s]))
        min_values.append(np.min(state_data[s]))
        max_values.append(np.max(state_data[s]))

    plt.bar(
            range(len(medians)),
            [x/simulated_packets for x in medians],
            yerr = (
                [(medians[idx] - x)/simulated_packets for idx,x in enumerate(min_values)],
                [(x - medians[idx])/simulated_packets for idx,x in enumerate(max_values)] 
                ),
            capsize = 3,
            lw=2,
            label='Nejauša secība'
            )
    # plt.scatter(range(len(h0_values)),[x/100000 for x in h0_values])
    plt.plot(range(-100, 100),list(set([x/simulated_packets for x in h1_values]))*200, lw=2, label='Pferscher et al. rīks', linestyle='dashed', color='black')
    for idx, val in enumerate(max_values):
        med_val = medians[idx]
        if int(np.ceil(med_val)) == int(np.floor(med_val)):
            med_val = int(med_val)
        plt.annotate(text = f'{med_val}', xy=(idx, val/simulated_packets+0.01), ha='center')
    # plt.scatter(range(len(h1_values)),[x/100000 for x in h1_values])
    # plt.plot(range(len(h2_values)),[x/100000 for x in h2_values], lw=2, label='Pferscher et al. pēc modificētās paketes')
    # plt.scatter(range(len(h2_values)),[x/100000 for x in h2_values])
    plt.legend()
    if len(states) == 2:
        plt.xlim(-4, 4)
    else:
        plt.xlim(-1, len(states))
    plt.xticks(range(len(states)), states)
    plt.xlabel('Stāvokļa ID')
    plt.ylabel('Stāvokļa apmeklēšanas biežums')
    return

with open('results/pebble/pebble.100k_by_1k.json') as fh:
    h0 = json.load(fh)

with open('results/pebble/pebble.fuzzer10000k_before.json') as fh:
    h1 = json.load(fh)

# with open('results/pebble/pebble.fuzzer10000k_after.json') as fh:
    # h2 = json.load(fh)

h0_values = list(h0[0].values())
h1_values = list(h1[0].values())

# d = aggregate_data(h0)

# np.median(d['s0'])
# np.median(d['s1'])

draw_plot(h0, h1_values)
# plt.show()
plt.savefig('pebble_sim.png', dpi=300)
plt.close()

# plt.show()

# for i in range(10):
    # state = f's{i}'
    # state_values = [x.get(state,0) for x in h0]
    # plt.boxplot(state_values, positions=[i])
# plt.show()

with open('results/vivosmart4/vivosmart4-prepaired-vanilla-run-20240507-1.100k_by_1k.json') as fh:
    h0 = json.load(fh)

with open('results/vivosmart4/vivosmart4-prepaired-vanilla-run-20240507-1.fuzzer10000k_before.json') as fh:
    h1 = json.load(fh)

# with open('results/vivosmart4-prepaired-vanilla-run-20240507-1.fuzzer10000k_after.json') as fh:
    # h2 = json.load(fh)

h0_values = list(h0[0].values())
h1_values = list(h1[0].values())
draw_plot(h0, h1_values)
plt.savefig('vivo4_sim.png', dpi=300)
plt.close()



with open('results/itag2/itag2-20240515-1.100k_by_1k.json') as fh:
    h0 = json.load(fh)

with open('results/itag2/itag2-20240515-1.fuzzer10000k_before.json') as fh:
    h1 = json.load(fh)

# with open('results/itag2/itag2-20240515-1.fuzzer10000k_after.json') as fh:
    # h2 = json.load(fh)

h0_values = list(h0[0].values())
h1_values = list(h1[0].values())
draw_plot(h0, h1_values)
plt.savefig('itag2_sim.png', dpi=300)
plt.close()



