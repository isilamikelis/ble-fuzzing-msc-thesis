require 'timeout'

vulnerabilities = %w(
connection_interval_crash.py
connection_latency_crash.py
connection_timeout_crash.py
consecutive_connection_crash.py
length_rsp_terminates_connection.py
length_unexpected_state.py
multiple_version_ind_test.py
pairing_max_key_size_greater_than_spec.py
pairing_max_key_size_test.py
)

allowed_test_time = Hash.new(60)
allowed_test_time["extras/knob_tester_ble.py"] = 10000
allowed_test_time["extras/non_compliance_data_during_encryption_setup.py"] = 10000
# allowed_test_time["extras/Microchip_and_others_non_compliant_connection.py"] = 10000

address = "B8:27:EB:42:83:75" # raspberry
address = "E0:80:8D:FB:D8:24" # fitbit extras/DA14680_att_mtu_length_5_malformed_accepted.py
                              # fitbit extras/CC2540_connection_req_crash_truncated.py
address = "FF:FF:00:00:38:F5" # MLE-15 extras/CC2540_truncated_connection_success.py 
                              # extras/Microchip_and_others_non_compliant_connection.py
address = "C6:16:96:A9:91:FC" # vivosmart4 knob tester
address = "90:f1:57:85:0b:d1" # venu 2 plus llid-deadlock, knob
                              # extras/non_compliance_data_during_encryption_setup.py
                              # extras/non_compliance_nonzero_ediv_rand.py
address = "D1:13:CE:32:E1:A0" # pebble Telink_zero_ltk_installation.py
port = "/dev/ttyACM0"

vulnerabilities.each do |vuln|
  cmd = "python3 #{vuln} #{port} #{address}"
  pid = Process.spawn(cmd)
  puts "Running #{cmd}"
  begin
  test_time = allowed_test_time[vuln]
  Timeout.timeout(test_time) do
    Process.wait(pid)
    puts "Process exited with status: #{$?.exitstatus}"
  end
  rescue Timeout::Error
    Process.kill('TERM', pid)
    puts "Process killed due to timeout."
  end
  sleep 5
end

# python2.7 llid_dealock.py /dev/ttyACM0 $addr # OK
# python2.7 link_layer_length_overflow.py  /dev/ttyACM0 $addr
# python2.7 CC2640R2_public_key_crash.py  /dev/ttyACM0 $addr
# python2.7 DA14680_exploit_silent_overflow.py /dev/ttyACM0 $addr # OK
# python2.7 extras/knob_tester_ble.py /dev/ttyACM0 $addr # izskatās ka OK
# python2.7 extras/CC2540_connection_req_crash_truncated.py /dev/ttyACM0 $addr # izskatās ka OK
# python2.7 extras/non_compliance_multiple_version.py /dev/ttyACM0 $addr # izskatās ka OK
# python2.7 extras/CC2540_truncated_connection_success.py /dev/ttyACM0 $addr # izskatās ka OK?
# python2.7 extras/Microchip_and_others_non_compliant_connection.py /dev/ttyACM0 $addr # izskatās ka OK?
# python2.7 extras/DA14680_truncated_l2cap_crash.py /dev/ttyACM0 $addr # izskatās ka OK?
# python2.7 extras/DA14680_truncated_l2cap_crash.py /dev/ttyACM0 $addr # izskatās ka OK?

