# Izmantotais _Pferscher et al._ rīks, lai testētu ierīci Vivosmart 4 un Pebble Keys 380S
Automātu rekonstrukcijai un testēšanai nekādas modifikācijas nav veiktas

Veicot Pebble Keys 380S nejaušdatu testēšanas etapus, ar neatkarīgu kontrolieri
ik pēc 60 sekundēm tika sūtīta pakešu virkne, lai ierīci noturētu reklāmrežīmā,
izmantojot palīgskriptu [ble_ping.py](https://gitlab.com/isilamikelis/ble-fuzzing-msc-thesis/main/tools/ble-fuzzing-utils/ble_ping.py)