import sys

from fuzzing.FuzzingBLESUL import FuzzingBLESUL,FuzzedParam
import constant
from FuzzingReport import FuzzingReport


serial_port = "/dev/ttyACM2"
advertiser_address = "FF:FF:2D:9F:E2:80" # itag1
fuzzing_report_fname = 'sample_input/fuzzing_report.txt'

serial_port = sys.argv[1]
advertiser_address = sys.argv[2]
fuzzing_report_fname = sys.argv[3]
pcap_outname = sys.argv[4]

fuzzing_sul = FuzzingBLESUL(serial_port, advertiser_address)

alphabet = ['scan_req', 'connection_req', 'length_req', 'length_rsp',  'feature_rsp', 'version_req', 'mtu_req', 'pairing_req', 'feature_req']

# itag2 crash
fuzzing_report = FuzzingReport(fuzzing_report_fname)
last_fuzz_rec = fuzzing_report.records[-1]
prefix = eval(last_fuzz_rec.prefix)
fuzzparam = last_fuzz_rec.fuzzed_param
fuzzed_param = (last_fuzz_rec.fuzzed_pkt+'_fuzzed', fuzzparam)
suffix = eval(last_fuzz_rec.suffix)

# pebble crash testing params
# prefix: ('connection_req', 'version_req'), fuzzing inputs: ['connection_req[interval: 61388]'], after fuzzing suffix: []

# prefix = ('connection_req', 'version_req')
# fuzzed_param = ('connection_req_fuzzed', FuzzedParam('interval', 61388))
# suffix = tuple()

attempt_threshold = 3

fuzzing_sul.pre()
for pkt in prefix:
    stepval = constant.ERROR
    attempt = 1
    while stepval == constant.ERROR and attempt <= constant.CONNECTION_ERROR_ATTEMPTS:
        print(f'prefix attempt {attempt}')
        stepval = fuzzing_sul.step(pkt)
        attempt += 1

stepval = constant.ERROR
attempt = 1
while stepval == constant.ERROR and attempt <= constant.CONNECTION_ERROR_ATTEMPTS:
    print(f'fuzzing attempt {attempt}')
    stepval = fuzzing_sul.step(fuzzed_param[0], fuzzed_param[1])
    attempt += 1

for pkt in suffix:
    stepval = constant.ERROR
    attempt = 1
    while stepval == constant.ERROR and attempt <= constant.CONNECTION_ERROR_ATTEMPTS:
        print(f'suffix attempt {attempt}')
        stepval = fuzzing_sul.step(pkt)
        attempt += 1
fuzzing_sul.post()

fuzzing_sul.save_pcap(pcap_outname)
