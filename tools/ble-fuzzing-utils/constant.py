CONNECTION_ERROR_ATTEMPTS = 20
NON_DET_ERROR_ATTEMPTS = 20
NON_DET_CACHE_SIZE = 20
SCAN_MIN_ATTEMPTS = 5
SCAN_MAX_ATTEMPTS = 100
CONNECT_MIN_ATTEMPTS = 10
CONNECT_MAX_ATTEMPTS = 100
TERMINATE_MIN_ATTEMPTS = 2
TERMINATE_MAX_ATTEMPTS = 5
MIN_ATTEMPTS = 20
MAX_ATTEMPTS = 30
PHYSICAL_RESET = False
ERROR = 'ERROR'
LOG_PCAP = True
MAX_FUZZING_CEX_REPETITIONS = 5


