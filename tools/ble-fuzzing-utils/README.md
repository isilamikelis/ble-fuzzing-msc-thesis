# Palīgskripti nejaušdatu testēšanas veikšanai

Izveidotie palīgskripti:
`ble_ping.py`: periodiski ierīcei nosūta `.pre()` un `.post()` pakešu sekvences.

Izmantojuma piemērs, lai ierīce ik pēc 60 sekundēm tiktu nosūtītas pakešu virknes.
```
python3 ble_ping.py /dev/ttyACM0 FF:FF:2D:9F:E2:80 60
```
