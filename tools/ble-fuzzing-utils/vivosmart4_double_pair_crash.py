import sys
import constant
import resource
from BLESUL import BLESUL
from FailSafeLearning.StatePrefixEqOracleFailSafe import StatePrefixOracleFailSafe
from FailSafeLearning.FailSafeCacheSUL import FailSafeCacheSUL
from aalpy.learning_algs import run_Lstar
from aalpy.utils import visualize_automaton
from util import print_error_info

rsrc = resource.RLIMIT_DATA
soft, hard = resource.getrlimit(rsrc)
resource.setrlimit(rsrc, (1024 * 1024 * 1024 * 12, hard))

serial_port = sys.argv[1]
advertiser_address = sys.argv[2]

ble_sul = BLESUL(serial_port, advertiser_address)

for _ in range(10):
    ble_sul.step('scan_req')
    ble_sul.step('connection_req')
    ble_sul.step('pairing_req')
    ble_sul.step('pairing_req')


