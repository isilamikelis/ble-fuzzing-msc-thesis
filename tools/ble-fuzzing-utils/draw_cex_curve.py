import matplotlib.pyplot as plt
from collections import Counter

class CexRec:
    def __init__(self, cex):
        fields = cex.split(' and ')
        fields[0] = fields[0].replace("Error inserting prefix: ", "")
        self.prefix = fields[0]
        if len(fields) == 3:
            fields[1] = fields[1].replace("fuzzed suffix ", "")
            fields[2] = fields[2].replace("suffix ", "")
            self.fuzzed_suffix = fields[1]
            self.suffix = fields[2]
        else:
            fields[1] = fields[1].replace("suffix ", "")
            self.fuzzed_suffix = fields[1]
            self.suffix = ""
    def __repr__(self):
        return f'{self.prefix} {self.fuzzed_suffix} {self.suffix}'

class RepRec:
    def __init__(self, rep):
        self.prefix = rep.split(', fuzzing inputs: ')[0].replace("prefix: ","")
        self.fuzzed_suffix = rep.split(', fuzzing inputs: ')[1].split(', after fuzzing suffix: ')[0]
        self.suffix = '['+rep.split(', after fuzzing suffix: ')[1][1:-1]+']'
        if self.suffix[-2] == ',':
            self.suffix = self.suffix[:-2] + ']'

    def __repr__(self):
        return f'{self.prefix} {self.fuzzed_suffix} {self.suffix}'


def load_content(fname):
    with open(fname) as fh:
        content = [x.strip() for x in fh.readlines()]
    return content

def next_cex_chunk(cont, offset):
    i = offset
    chunk = []
    while i < len(cont) and len(cont[i]) == 0:
        i += 1

    line_count = 0
    while i < len(cont) and line_count < 3:
        if '-----' in cont[i]:
            line_count += 1
        chunk.append(cont[i])
        i += 1
    cont = cont[i:]
    return chunk, i

def get_cex_sequences(cex_report_cont):
    result = []
    offset = 0
    while offset < len(cex_report_cont):
        chunk, offset = next_cex_chunk(cex_report_cont, offset)
        if 'state cannot be calculated' in ''.join(chunk):
            continue
        if len(chunk) == 0:
            continue
        result.append(chunk[2])
    return result 

def get_full_sequences(full_report_cont):
    return [x 
            for x in full_report_cont
            if "prefix: (" in x]


def cex_corresponds_to_rep(cex, rep):
    if len(cex.suffix) > 0:
        return cex.prefix == rep.prefix and \
            cex.fuzzed_suffix == rep.fuzzed_suffix and \
            cex.suffix == rep.suffix
    else:
        return cex.prefix == rep.prefix and \
            cex.fuzzed_suffix == rep.fuzzed_suffix


def get_cex_count_data(cex_report_fname, full_report_fname):
    # full_report_fname = 'fuzzing_report.txt'
    # cex_report_fname = 'fuzzing_cex_report.txt'
    full_report_cont = load_content(full_report_fname)
    cex_report_cont = load_content(cex_report_fname)
    cex_seq = get_cex_sequences(cex_report_cont)
    full_seq = get_full_sequences(full_report_cont)
    len(set(cex_seq))
    len(cex_seq)
    Counter(cex_seq)
    cex_i = 0
    rep_i = 0
    curr_cex_count = 0
    rep_indices = []
    cex_counts = []
    while cex_i < len(cex_seq) and rep_i < len(full_seq):
        curr_cex = CexRec(cex_seq[cex_i])
        curr_rep = RepRec(full_seq[rep_i])
        if cex_corresponds_to_rep(curr_cex, curr_rep):
            curr_cex_count += 1
            print(f'{rep_i} {curr_cex_count} {cex_i}')
            # print(rep_i)
            # print(curr_cex)
            # print(curr_rep)
            cex_i += 1
        rep_indices.append(rep_i)
        cex_counts.append(curr_cex_count)
        rep_i += 1
    return rep_indices, cex_counts

if __name__ == '__main__':
    full_report_fname = 'fuzzing_report.txt'
    cex_report_fname = 'fuzzing_cex_report.txt'
    vivo4_rep, vivo4_cex = get_cex_count_data(cex_report_fname, full_report_fname)
    pebble_rep, pebble_cex = get_cex_count_data('pebble/fuzzing_cex_report.txt', 'pebble/fuzzing_report.txt')

    plt.plot(vivo4_rep, [x/10 for x in vivo4_cex], label='Vivosmart 4 (10)', lw=2)
    plt.plot(pebble_rep, [x/9 for x in pebble_cex], label='Pebble Keys 380S (9)', lw=2)
    plt.xlabel('Testa piemēra indekss')
    plt.ylabel('Kumulatīvais pretpiemēru skaits uz vienu stāvokli')
    plt.legend()
    plt.savefig('/var/home/ivars/Documents/Uni/magistraDarbs/text/pic/vivo_pebble_cex.png', dpi=300)
    plt.close()


