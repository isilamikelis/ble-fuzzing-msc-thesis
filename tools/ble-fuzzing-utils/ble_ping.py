import sys
import constant
import resource
from BLESUL import BLESUL
from FailSafeLearning.StatePrefixEqOracleFailSafe import StatePrefixOracleFailSafe
from FailSafeLearning.FailSafeCacheSUL import FailSafeCacheSUL
from aalpy.learning_algs import run_Lstar
from aalpy.utils import visualize_automaton
from util import print_error_info

rsrc = resource.RLIMIT_DATA
soft, hard = resource.getrlimit(rsrc)
resource.setrlimit(rsrc, (1024 * 1024 * 1024 * 12, hard))
import time


serial_port = sys.argv[1]
advertiser_address = sys.argv[2]
sleep_duration = int(sys.argv[3])

ble_sul = BLESUL(serial_port, advertiser_address)

if sleep_duration == "once":
    ble_sul.pre()
    ble_sul.post()
    sys.exit()
else:
    while True:
        try:
            ble_sul.pre()
            ble_sul.post()
        except:
            print("Device is not reachable")
