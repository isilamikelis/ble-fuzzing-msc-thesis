import sys
import constant
import resource
from BLESUL import BLESUL

rsrc = resource.RLIMIT_DATA
soft, hard = resource.getrlimit(rsrc)
resource.setrlimit(rsrc, (1024 * 1024 * 1024 * 12, hard))

alphabet = ['scan_req', 'connection_req', 'length_req', 'length_rsp',  'feature_rsp', 'feature_req', 'version_req', 'mtu_req', 'pairing_req']

def perform_request(ble_sul, req):
    packet_names = {
            "TX ---> BTLE / BTLE_ADV / BTLE_SCAN_REQ" : "scan_req",
            "TX ---> BTLE / BTLE_DATA / BTLE_CTRL / LL_TERMINATE_IND" : "post",
            "TX ---> BTLE / BTLE_ADV / BTLE_CONNECT_REQ" : "connection_req",
            "TX ---> BTLE / BTLE_DATA / BTLE_CTRL / LL_FEATURE_REQ" : "feature_req",
            "TX ---> BTLE / BTLE_DATA / BTLE_CTRL / LL_FEATURE_RSP" : "feature_rsp",
            "TX ---> BTLE / BTLE_DATA / BTLE_CTRL / LL_LENGTH_REQ" : "length_req",
            "TX ---> BTLE / BTLE_DATA / BTLE_CTRL / LL_LENGTH_RSP" : "length_rsp",
            "TX ---> BTLE / BTLE_DATA / BTLE_CTRL / LL_VERSION_IND" : "version_req",
            "TX ---> BTLE / BTLE_DATA / L2CAP_Hdr / ATT_Hdr / ATT_Exchange_MTU_Request" : "mtu_req",
            "TX ---> BTLE / BTLE_DATA / L2CAP_Hdr / SM_Hdr / SM_Pairing_Request" : "pairing_req",
            }
    next_step = packet_names[req]
    if next_step == "post":
        ble_sul.post()
    else:
        ble_sul.step(next_step)
    return

def replay_packet_sequence(ble_sul, fname, last_n):
    seq_oi = []
    with open(fname) as fh:
        seq_oi = [x.strip() for x in fh.readlines() if "TX --->" in x]
    if last_n == "all":
        last_n_packets = seq_oi
    else:
        last_n = int(last_n)
        last_n_packets = seq_oi[-last_n:]
    while not "LL_TERMINATE_IND" in last_n_packets[0]:
        last_n_packets.pop(0)
    last_n_packets.pop(0)
    for req in last_n_packets:
        perform_request(ble_sul, req)
    return

serial_port = sys.argv[1]
advertiser_address = sys.argv[2]
logfile = sys.argv[3]
lastn = sys.argv[4]

ble_sul = BLESUL(serial_port, advertiser_address)

replay_packet_sequence(ble_sul, logfile, lastn)
